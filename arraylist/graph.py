import math
import pandas as pd
from math import pi
import numpy as np
import re
import matplotlib.pyplot as plt

file = open("data", "r")
fileContents = file.readlines()
sampleSize = []
timeTaken = []

for line in fileContents:
    lineData = re.findall("\d+", line)
    # print(lineData)
    sampleSize.append(lineData[0])
    timeTaken.append(lineData[1])

dataFrame = pd.DataFrame({
    "samples": np.array(sampleSize),
    "time": np.array(timeTaken)
})

# print(sampleSize)

print(dataFrame.describe())
# dataFrame.plot()

plt.plot(sampleSize, timeTaken)

#ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
# ts.plot()
# print(ts.values)
file.close()
