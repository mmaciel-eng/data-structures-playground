const arrayList = require("./arraylist");
const fs = require("fs");

const testArray = new arrayList();

const file = fs.createWriteStream("data");
const sampleData = 1000000;
const stepSize = 10000;

for (let index = stepSize; index <= sampleData; index += stepSize) {
  const start = new Date().getTime();
  let testArray = new arrayList();

  // console.log(`Starting at ${start} for ${index} items`);

  for (let testQuantity = 0; testQuantity < index; testQuantity++) {
    testArray.append({ testQuantity });
  }

  const end = new Date().getTime() - start;

  // console.log(`Finished for ${index} elements after (ms): ${end}`);

  testArray = null;

  file.write(`quantity: ${index - 1}; end: ${end}\n`);
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write(`At ${(index / sampleData) * 100}% progress...`);
}

process.stdout.clearLine();
process.stdout.cursorTo(0);
process.stdout.write(`done!`);
file.end();

// console.log(`Get for 9999: ${JSON.stringify(testArray.get(9999))}`);
// console.log(`Get for 0: ${JSON.stringify(testArray.get(0))}`);
// console.log(`Get for 1: ${JSON.stringify(testArray.get(1))}`);
// console.log(`Get for 10000: ${JSON.stringify(testArray.get(10000))}`);
// console.log(`Get for 10001: ${JSON.stringify(testArray.get(10001))}`);
