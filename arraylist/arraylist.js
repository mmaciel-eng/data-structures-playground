class ArrayList {
  constructor() {
    this.items = new Array(8);
    this.size = 0;
  }

  size() {
    return this.size;
  }

  set(index, items) {
    if (index > this.size || index < 0) {
      throw new Error("Out of bounds!");
    }

    this.items = items;
  }

  append(item) {
    if (this.size > this.items.length) {
      const newArray = [...this.items, new Array(this.items.length / 2)];
      this.items = newArray;
    }

    this.items[this.size] = item;
    this.size++;
  }

  get(index) {
    if (index > this.size || index < 0) {
      throw new Error("Out of bounds!");
    }

    return this.items[index];
  }
}

module.exports = ArrayList;
